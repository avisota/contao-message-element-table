<?php

/**
 * Avisota newsletter and mailing system
 * Copyright © 2016 Sven Baumann
 *
 * PHP version 5
 *
 * @copyright  way.vision 2016
 * @author     Sven Baumann <baumann.sv@gmail.com>
 * @package    avisota/contao-core
 * @license    LGPL-3.0+
 * @filesource
 */

/**
 * Fields
 */
$GLOBALS['TL_LANG']['orm_avisota_message_content']['tableItems']   = array(
    'Table Entries ',
    'If JavaScript is disabled, you must save your changes until you change the order.'
);
$GLOBALS['TL_LANG']['orm_avisota_message_content']['tableSummary'] = array(
    'Summary ',
    ' Please enter a brief summary of the content and structure of the table.'
);
$GLOBALS['TL_LANG']['orm_avisota_message_content']['tableHeader']  = array(
    'Add header',
    'The first row of the table to use as the header.'
);
$GLOBALS['TL_LANG']['orm_avisota_message_content']['tableFooter']  = array(
    'Add footer',
    'The last row of the table to use as a footer.'
);

/**
 * Legends
 */
$GLOBALS['TL_LANG']['orm_avisota_message_content']['table_legend']   = 'Table Entries';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['tconfig_legend'] = 'Table Settings';
