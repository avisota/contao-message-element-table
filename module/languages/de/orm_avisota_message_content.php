<?php

/**
 * Translations are managed using Transifex. To create a new translation
 * or to help to maintain an existing one, please register at transifex.com.
 *
 * @link    http://help.transifex.com/intro/translating.html
 * @link    https://www.transifex.com/projects/p/avisota-contao/language/de/
 *
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 *
 * last-updated: 2014-03-25T14:18:01+01:00
 */

$GLOBALS['TL_LANG']['orm_avisota_message_content']['tableFooter']['0']  = 'Tabellenfuß hinzufügen';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['tableFooter']['1']  = 'Die letzte Zeile der Tabelle bildet die Fußzeile.';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['tableHeader']['0']  = 'Tabellenkopf hinzufügen';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['tableHeader']['1']  = 'Die erste Zeile der Tabelle bildet die Kopfzeile.';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['tableItems']['0']   = 'Tabelleneinträge';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['tableItems']['1']   = 'Falls Javascript ausgeschaltet ist sichern Sie Ihre Änderungen, bevor Sie die Reihenfolge der Elemente verändern.';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['tableSummary']['0'] = 'Zusammenfassung';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['tableSummary']['1'] = 'Bitte geben Sie für eine barrierearme Tabelle eine kurze Zusammenfassung an und beschreiben dabei den Zweck oder die Struktur.';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['table_legend']      = 'Tabellenelemente';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['tconfig_legend']    = 'Tabellen-Einstellungen';
