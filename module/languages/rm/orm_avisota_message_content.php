<?php

/**
 * Translations are managed using Transifex. To create a new translation
 * or to help to maintain an existing one, please register at transifex.com.
 *
 * @link    http://help.transifex.com/intro/translating.html
 * @link    https://www.transifex.com/projects/p/avisota-contao/language/rm/
 *
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 *
 * last-updated: 2014-03-11T16:03:42+01:00
 */

$GLOBALS['TL_LANG']['orm_avisota_message_content']['tableFooter']['0']  = 'Agiuntar in pe da la tabella';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['tableFooter']['1']  = 'Far ord l\'ultima colonna il pe da la tabella.';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['tableHeader']['0']  = 'Agiuntar in chau da tabella';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['tableHeader']['1']  = 'Far ord l\'emprima lingia il chau da la tabella.';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['tableItems']['0']   = 'Elements da la tabella';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['tableItems']['1']   = 'Sche JavaScript è deactivà stos ti memorisar tias midadas avant che midar la successiun.';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['tableSummary']['0'] = 'Resumaziun';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['tableSummary']['1'] = 'Endatescha ina curta resumaziun da la tabella e descriva la finamira e structura.';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['table_legend']      = 'Elements da la tabella';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['tconfig_legend']    = 'Configuraziun da la tabella';
